# listscroll.py
#
# Copyright 2019 Keno Goertz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

def adjust_scrolling(listbox, num_max_rows):
    scrolled_window = listbox.get_parent().get_parent()
    rows = listbox.get_children()
    if len(rows) <= num_max_rows:
        scrolled_window.set_min_content_height(-1)
        scrolled_window.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)
    else:
        height = 0
        for i in range(num_max_rows):
            height += rows[i].get_preferred_height()[0]
        scrolled_window.set_min_content_height(height)
        scrolled_window.set_policy(
            Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
