# window.py
#
# Copyright 2019 Keno Goertz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from gi.repository import Gtk

from .wrappers.borgrepo import BorgRepo
from .helpers.listscroll import adjust_scrolling


@Gtk.Template(resource_path='/com/gitlab/kenogo/Morlock/ui/window.ui')
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'MorlockWindow'

    open_repo_button = Gtk.Template.Child()
    init_repo_button = Gtk.Template.Child()
    backups_box = Gtk.Template.Child()
    backups_list_box = Gtk.Template.Child()
    list_add_button = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.backups_box.set_visible(False)
        self.list_add_button.set_visible(False)
        self.borg_repo = None

    @Gtk.Template.Callback()
    def on_repo_file_set(self, button):
        repo_path = self.open_repo_button.get_filename()
        self.borg_repo = BorgRepo(repo_path)
        status = self.borg_repo.list()
        if status == BorgRepo.STATUS_SUCCESS:
            self.depopulate_backups_list()
            self.populate_backups_list()
        else:
            self.borg_repo = None
            dialog = MarkupMessageDialog(
                self, Gtk.MessageType.ERROR,
                "<b>Failed to read backups from chosen location</b>",
                "<i>%s</i> may not be a valid backup location. (%d)"
                % (repo_path, status))
            dialog.run()
            dialog.destroy()
            self.depopulate_backups_list()
            # Sadly, self.open_repo_button.unselect_all() by itself doesn't
            # change the selection to "(None)", so we need to use this hack
            # (technically, it is possible to have a folder that has only a
            # newline character as its name. However, people who do crazy stuff
            # like this with their computers should probably be used to
            # experiencing weird bugs :D)
            self.open_repo_button.set_current_folder("\n")
            self.open_repo_button.unselect_all()

    @Gtk.Template.Callback()
    def on_init_repo_button_clicked(self, button):
        file_chooser = Gtk.FileChooserDialog(
            "Choose Location for New Backup Repo", self,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = file_chooser.run()

        repo_path = None
        if response == Gtk.ResponseType.OK:
            repo_path = file_chooser.get_filename()
            self.borg_repo = BorgRepo(repo_path)

        file_chooser.destroy()

        # This needs to be run separately so we can close the file chooser
        # dialog before opening up another one notifying us of the success
        # or failure of initializing the repo. We check on repo_path rather
        # than, for example, self.borg_repo, so that hitting cancel on the file
        # chooser won't change anything
        if repo_path:
            status = self.borg_repo.init()
            if status == BorgRepo.STATUS_SUCCESS:
                dialog = MarkupMessageDialog(
                    self, Gtk.MessageType.INFO,
                    "<b>Successfully created a new backup location</b>",
                    "%s" % repo_path)
                dialog.run()
                dialog.destroy()
                self.open_repo_button.set_filename(self.borg_repo.path)
                self.borg_repo.list()
                self.populate_backups_list()
            elif status == BorgRepo.STATUS_PATH_NOT_EMPTY:
                dialog = MarkupMessageDialog(
                    self, Gtk.MessageType.ERROR,
                    "<b>Failed to create backup location</b>",
                    "<i>%s</i> is not empty." % repo_path)
                dialog.run()
                dialog.destroy()
            else:
                dialog = MarkupMessageDialog(
                    self, Gtk.MessageType.ERROR,
                    "<b>Failed to create backup location</b>",
                    "Received unknown status code: %d" % status)
                dialog.run()
                dialog.destroy()

    @Gtk.Template.Callback()
    def on_list_add_button_clicked(self, button):
        print("TODO: on_list_add_button_clicked")

    def populate_backups_list(self):
        self.list_add_button.set_visible(True)
        if len(self.borg_repo.archives) == 0:
            return
        self.backups_box.set_visible(True)
        for entry in self.borg_repo.archives:
            name = entry["name"]
            time = datetime.strftime(entry["time"], "%x (%X)")
            backups_list_entry = BackupsListEntry(name, time)
            self.backups_list_box.prepend(backups_list_entry)
        adjust_scrolling(self.backups_list_box, 5)

    def depopulate_backups_list(self):
        self.list_add_button.set_visible(False)
        self.backups_box.set_visible(False)
        for entry in self.backups_list_box.get_children():
            entry.destroy()
        self.resize(1, 1)


class MarkupMessageDialog(Gtk.MessageDialog):
    def __init__(self, parent, message_type, text, secondary_text=None):
        super().__init__(parent, 0, message_type, Gtk.ButtonsType.OK, "")
        self.get_message_area().get_children()[0] \
            .set_justify(Gtk.Justification.CENTER)
        self.set_markup(text)
        self.format_secondary_markup(secondary_text)


class BackupsListEntry(Gtk.ListBoxRow):
    def __init__(self, name, time):
        super().__init__()
        self.set_visible(True)

        self.hbox = Gtk.HBox()
        self.hbox.set_visible(True)
        self.hbox.set_spacing(12)
        self.hbox.set_margin_top(6)
        self.hbox.set_margin_bottom(6)
        self.hbox.set_margin_left(6)
        self.hbox.set_margin_right(6)
        self.add(self.hbox)

        self.name = Gtk.Label(name)
        self.name.set_visible(True)
        self.name.set_halign(Gtk.Align.START)
        self.hbox.add(self.name)

        self.time = Gtk.Label(time)
        self.time.set_visible(True)
        self.time.set_halign(Gtk.Align.END)
        self.hbox.add(self.time)
